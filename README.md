# KeeOtp

This is a plugin for the password management program KeePass. It adds the ability to generate timed one time passwords (Timed One Time Passwords or TOTP) from a secret key that is stored in the KeePass database.

This plugin is released under an MIT License which is compatible with the GNU GPL GNU Compatible License List (referred to as Expat in the GNU GPL compatibility list) that KeePass is released under.

It is based largely on a library called OTP-Sharp which does all the heavy lifting in calculating one time passwords. OtpSharp is also released under an MIT License.

## Disclaimer
This software is released under an MIT license. See the file LICENSE.txt for the full license.

Every effort has gone into ensuring that KeeOtp generates correct TOTP codes and that KeeOtp is bug free and will preserve your secret key.

Nevertheless it is up to you to Ensure that all measures are taken to safeguard your key.

Most systems that rely on TOTP are very hard to unlock if you lose your secret key. In the case of Google 2-step verification you should ensure that you have backup options (SMS, printed list of one time codes) in the unlikely event that KeeOtp fails to preserve your key. Dropbox provides an unlock code that disables this feature. These measures are your responsibility.

Other systems may have different options and it is your responsibility to preserve whatever data may be needed if KeeOtp should fail. This may be in another entry in KeePass or another hardened store of some kind.

## Feedback
If you encounter any bugs or want to see a feature added, feel free to open a ticket under the issues tab. Please search to see if it already exists first. If so, you can add a comment instead.

## Compatibility
This plugin supports the TOTP standard and should work with any service that is compliant with RFC 6238 and uses SHA1 as the HMAC hashing algorithm. SHA-256 and SHA-512 are not currently supported but likely will be in the near future. It supports any specified time step and will generate 6 or 8 digit codes.

*Warning. Only change the time step if the service that you are using this with tells you to. It won't work if your time step is different from theirs.*

The key must be provided in base32. If the service doesn't provide the shared secret key in base32 (most do) then it must be converted first.

This plugin supports generating one time passwords for

 - Google 2 Step Verification
 - Amazon Web Services Multi Factor Authentication
 - Dropbox two-step verification
 - Facebook
 - Any standard TOTP (Timed One Time Password) implementation that uses SHA1 as the HMAC hashing algorithm (the underlying library supports all hashes in the TOTP RFC specification so other hashing algorithms are possible) that let you have the secret key.

## Installation
To install simply drop the KeeOtp.dll and the OtpSharp.dll in the root of your KeePass directory. The dlls can be obtained either by building the source yourself using msbuild or by downloading the latest zip file on the downloads section of this site.

## Use
This works by storing a shared secret key in your encrypted KeePass database and using that information coupled with the current time to generate rolling codes that can be entered into the verification system. You will need to generate a current code from the key stored in the KeePass database each time you need to re-authenticate.

## Initial Setup
When enabling TOTP on your verification system, you will be provided with a key. Often this comes in the form of a QR code. In most cases you can also get a base32 encoded key as well. This can be of varying lengths. Once the plugin is installed, every entry in KeePass will have a new option labeled "One Time Password" in the context menu of the entry.

Right click on the entry that you wish to add TOTP to (or create a new one) and select the "One Time Password" option. If there is no key associated with that entry, you will be taken to a form where you can enter it.

Paste the key into the textbox and click OK. You should now be looking at a rolling time based authentication code for that key.

## Clock Drift
KeeOtp relies on the current time in order to accurately produce a TOTP verification code. If your clock is off significantly from the clock of the server then they may not accept the code. You should ensure that your system clock is set correctly. TOTP is a rather precise operation so an incorrect time by even a minute will likely render your codes invalid (unless the verifying system accepts a wide range of codes). Additionally the time zone setting must also be correct since TOTP codes are calculated against UTC. If your timezone setting (or daylight savings setting) is incorrect the time could differ from the verifying system's time by hours.

There is currently no setting to apply a time correction factor to the code generation so it is up to you to ensure a correct system time.

## Obtaining an authentication code
Simply right click on the entry in KeePass and select "One Time Password" You will be shown the current password as well as the amount of time that it will remain current (the service may choose to accept a range of codes in addition to the current code.)

Alternatively you can select the "Copy TOTP to Clipboard" option to put the current TOTP in the clipboard. It will be automatically cleared as per your KeePass clipboard settings.

## Auto Type
As of version 1.0.4 there is a custom placeholder that allows a TOTP code to be entered into the system with the KeePass auto type system. To configure this go into the settings of your KeePass entry that contains your TOTP key. Navigate to the Auto-Type tab. Configure your custom sequence with the placeholder {totp}. The {totp} will be replaced with your current totp authentication code.

## Troubleshooting
There are many things that can cause an incorrect code to be generated. We'll go through them here on this page.

### Clock
The most common reason for an incorrect code is for the client and the server to have different time settings. This is because the code generated is based on the system time and changes every thirty seconds. Most verifying systems will only accept the current code, previous code, and the next code. If your time is off by more than 30 seconds you stand a good chance of generating codes outside of the window that will be accepted by the server.

 1. Check that your system time is correct. If your computer supports auto setting of the clock based on a network time server than this is a good way to ensure that everything correct.

 2. Check your timezone and daylight savings settings. These codes are generated using UTC. If your timezone setting is off but your time is correct for the local time then your UTC time will be off by as many hours as your timezone is off. The codes generated in this case will be completely incorrect.

 3. Check that your date is correct. If the time is correct but the date is off the code will still be wrong. Each 30 second increment that elapses is considered a new code window. The same code isn't generated at the same time each day but rather a new code is generated every thirty seconds.

 4. Be sure that the verifying system uses UTC to generate codes. The RFC that defines the TOTP standard used here recommends UTC but that doesn't mean that the verifying system uses UTC. If they don't use UTC then KeeOtp will not work since KeeOtp always uses UTC. There is a feature request to add this capability in a future version. See issue #8 to follow this request.


### Clock Test
If you are using the Google Authenticatior smartphone app successfully there is a little test that you can do. Download the current OTP-Sharp package. Inside there should be a demonstration application that generates a QR code for the Google Authenticator app. Don't change any settings (or it may not work) and scan the QR code with your smartphone. If the code generated doesn't match what is on your phone then you likely still have a clock sync issue.

### Key
KeeOtp currently accepts keys as base32. Ensure that the key provided to you is correctly encoded as base32. The known systems that KeeOtp works with all provide the key as base32 already so no additional encoding should be needed.

It is possible however that the system provides the key in some other way. If this is the case you will need to convert it to base32 as that is currently the only way that KeeOtp accepts keys. There is a feature request for alternate encodings. See #7 to follow that request.

 1. Ensure that the key is encoded correctly. Look up the key encoding used by the service. Google, Amazon, and Dropbox all use base32 so no additional encoding is needed. The spaces will need to be stripped out.

 2. If your key is encoded using base64 or hex you will need to translate that to base32.


### Other Settings
There are several settings that can be specified when setting up the entry in KeeOtp. In addition to the key there is a step window and a digit size.

 1. The most common step window is 30 seconds. KeeOtp allows alternate time steps to be specified. Be sure that if an alternate time step is specified that it is because the verifying system has specifically said that is what they use. If the time step doesn't match the server's expected time step then the code will not ever be correct. When in doubt, leave this setting at the default 30 seconds.

2. Code size is another setting that can cause incorrect codes. The most common size is 6 but 8 is also allowed. Be sure that this number matches the verifying service's expectations. When in doubt leave this value at 6 as most services use a 6 digit code.


### Other Problems

Version 1.3.0 had a problem that caused an error to occur and crash the application with an error of

"PNG images with 64bpp aren't supported by libgdiplus."

This issue has been resolved with 1.3.1. It is possible, however, that even after updating this issue still persists when using PLGX files. To resolve this issue update to the latest version and clear the plugin cache. This can be done under Tools -> Plugins.



## Attributions

### Icons
The icons used in this plugin are from the Oxygen icon set and are used under the Creative Commons Attribution-NonCommercial-NoDerivs 2.5 Generic (CC BY-NC-ND 2.5) license.

The author's website is located here: http://www.oxygen-icons.org

### Contributors
[Devin Martin](https://bitbucket.org/devinmartin/)
[Joel Low](https://bitbucket.org/lowjoel/)
[Roc Ramon](http://rorlork.xyz)
