# Release Notes

## Beta Version
 - 1.3.12
   - Add support for stronger hash algorithms #17
   - Download From Build Server

## Current Version
 - 1.3.9
   - UI Improvements on form editing

## Previous versions
 - 1.3.7
   - Fix sync issue #24
 - 1.3.6
   - Show the {TOTP} auto-type placeholder in the placeholder edit form.
 - 1.3.1 (Bug fix)
   - Fixed incompatibility with *nix systems. Requires clearing the plugin cache for plgx files.
 - 1.3.0
   - Removed the radio buttons for HEX and base64 on the entry form as they were included accidentally
   - Major cosmetic improvements. Added banners and icons.
   - Allow spaces in the key entry form since many services provide a key with spaces included. It is now easier to copy those keys into the form directly.
 - 1.2.0
   - Added a menu item to the top level tools menu. On mac the right click context menu doesn't work so this plugin was unusable. All major functionality can now be used via the tools menu if desired.
   - Internal security improvements
 - 1.0.4 requires KeePass 2.20
   - Fixed bug uncaught exception when a context menu item was clicked without an entry selected
   - Added an auto type custom placeholder {totp} to allow TOTP codes to be auto typed.
   - Added a link to the troubleshooting page as well as a button to ping google for the current time (not authoritative like NIST but Google is probably the most common use for KeeOtp and they seem to have current time.)
 - 1.0.3
   - Fixed divide by zero bug error when entering a time step size of 0.
   - Added verification and warnings to the TOTP configuration dialog.
   - Removed all dependencies on System.Web so that users with a client profile installation can also use KeeOtp
   - Added a copy TOTP to clipboard menu item to the context menu.
 - 1.0.2 The initial version of KeeOtp which supports TOTP
   - Tested with Google 2-step verification
   - Tested with Amazon Web Services Multi Factor Authentication.
